### Versions: ###

App(beta):3.8.4.1
Bootstrap:3.3.6
Font-awesome:4.5.0
jQuery:2.1.4

### Sources ###

[BitBucket](https://bitbucket.org/bskydive/portfoliojs/src/62b71508d319?at=feature-3.8.3)

### Feature list ###

* Get weather from openweathermap.org json api
* Choose language: En/Ru
* Choose Units: Metric/Imperial
* Choose 3 nearest weather location points
* Get geo location from HTML5 API
* Show weather conditions
* Responsive mobile-first design
* Bootstrap styles


### Wish List: ###

* Show distance from current geo position to nearest weather location points
* Get weather from narodmon json api
* Get geo location from ipinfo.io/loc json api
* Choose geo location api
* Choose weather api
* Show weather history graphs: month, week, day
* Show weather forecast(openweathermap only)
* Show current lat,lon in app header
* Translation of the app header and title
* Make android client
* Determine adblock, noScript
* Determine disabed geolocation
* Disable weather get with city/lang/unit change(???) New refresh button(!!!)
* Add city name to tab title after weather refresh